#include <IRremote.h>

#define _led_pin 13
#define _light_analog_pin A0
#define _light_digital_pin 48
#define _ena 4
#define _in1a 50
#define _in2a 51

#define _enb 5
#define _in1b 52
#define _in2b 53

#define _ir_pin 2

IRrecv irrecv(_ir_pin);
decode_results _ir_results;

enum Movement
{
  starting,
  cruising,
  stopping,
  stopped
};
int minTrainSpeed = 100;
int maxTrainSpeed = 250;
int stepTrainSpeed = 25;
long trainInterval = 500;
long trainStopInterval = 5000;
unsigned long trainLastRead = 0;

int train1Speed = 0;
Movement train1Movement = stopped;
unsigned long train1Stopped = 0;
bool train1auto = false;

int train2Speed = 0;
Movement train2Movement = stopped;
unsigned long train2Stopped = 0;
bool train2auto = false;

void setup()
{
  // initialize digital pin 13 as an output.
  pinMode(_led_pin, OUTPUT);
  pinMode(_in1a, OUTPUT);
  pinMode(_in2a, OUTPUT);
  pinMode(_in1b, OUTPUT);
  pinMode(_in2b, OUTPUT);

  //attachInterrupt(0, startIRRead, FALLING);
  irrecv.enableIRIn();

  //Serial.begin(9600);
  //Serial.println("Startup.");
  trainLastRead = millis() - (trainInterval + 1000);
  train1Stopped = millis() - (trainStopInterval + 1000);
}

void loop()
{
  processDigitalLightSensor();
  //processAnalogLightSensor();
  runTrain1();

  if (irrecv.decode(&_ir_results))
  {
    word legoIR = getLegoIRWord(_ir_results);
    if (legoIR)
    {
      decodeLegoIR(legoIR);
    }
    irrecv.resume();
  }
}

void runTrain1()
{
  unsigned long currentMillis = millis();
  if (currentMillis - trainLastRead > trainInterval)
  {
    if (train1auto)
    {
      trainLastRead = currentMillis;
      if (train1Movement == stopped && (currentMillis - train1Stopped > trainStopInterval))
      {
        train1Movement = starting;
      }
      if (train1Movement == starting)
      {
        train1Speed += 20;
        if (train1Speed >= maxTrainSpeed)
        {
          train1Movement = cruising;
        }
      }
      if (train1Movement == stopping)
      {
        train1Speed -= 20;
        if (train1Speed <= 0)
        {
          train1Speed = 0;
          train1Movement = stopped;
          train1Stopped = currentMillis;
        }
      }
    }
    trainSpeed(1, train1Speed);
  }
}

void trainForward(int train)
{
  switch (train)
  {
    case 1:
      digitalWrite(_in1a, LOW);
      digitalWrite(_in2a, HIGH);
      break;
    case 2:
      digitalWrite(_in1b, LOW);
      digitalWrite(_in2b, HIGH);
      break;
  }
}

void trainBackward(int train)
{
  switch (train)
  {
    case 1:
      digitalWrite(_in2a, LOW);
      digitalWrite(_in1a, HIGH);
      break;
    case 2:
      digitalWrite(_in2b, LOW);
      digitalWrite(_in1b, HIGH);
      break;
  }
}

void trainStop(int train)
{
  switch (train)
  {
    case 1:
      digitalWrite(_in2a, LOW);
      digitalWrite(_in1a, LOW);
      break;
    case 2:
      digitalWrite(_in2b, LOW);
      digitalWrite(_in1b, LOW);
      break;
  }
}

void trainSpeed(int train, int s)
{
  if(s<0)
  {
    trainBackward(train);
  }
  else
  {
    trainForward(train);
  }
  if (s > maxTrainSpeed)
  {
    s = maxTrainSpeed;
  }
  if (s < (0-maxTrainSpeed))
  {
    s = (0-maxTrainSpeed);
  }
  switch (train)
  {
    case 1:
      train1Speed = s;
      s=abs(s);
      analogWrite(_ena, s);
      break;
    case 2:
      train2Speed = s;
      s=abs(s);
      analogWrite(_enb, s);
      break;
  }
}

void speedUpTrain(int train)
{
  //Serial.print("[TRAIN] >");
  //Serial.print(" Speed: ");
  switch (train)
  {
    case 1:
      train1Speed += stepTrainSpeed;
      if(train1Speed>(0-minTrainSpeed) && train1Speed<minTrainSpeed)
      {
        if(train1Speed<0)
        {
          train1Speed=0;
        }
        else
        {
          train1Speed=minTrainSpeed;
        }
      }
      //Serial.print(train1Speed);
      break;
    case 2:
      train2Speed += stepTrainSpeed;
      if(train2Speed>(0-minTrainSpeed) && train2Speed<minTrainSpeed)
      {
        if(train2Speed<0)
        {
          train2Speed=0;
        }
        else
        {
          train2Speed=minTrainSpeed;
        }
      }
      Serial.print(train2Speed);
      break;
  }
  //Serial.println("");
}

void slowDownTrain(int train)
{
  //Serial.print("[TRAIN] >");
  //Serial.print(" Speed: ");
  switch (train)
  {
    case 1:
      train1Speed -= stepTrainSpeed;
      if(train1Speed>(0-minTrainSpeed) && train1Speed<minTrainSpeed)
      {
        if(train1Speed>0)
        {
          train1Speed=0;
        }
        else
        {
          train1Speed=(0-minTrainSpeed);
        }
      }
      //Serial.print(train1Speed);
      break;
    case 2:
      train2Speed -= stepTrainSpeed;
      if(train2Speed>(0-minTrainSpeed) && train2Speed<minTrainSpeed)
      {
        if(train2Speed>0)
        {
          train2Speed=0;
        }
        else
        {
          train2Speed=(0-minTrainSpeed);
        }
      }
      Serial.print(train2Speed);
      break;
  }
  //Serial.println("");
}

void stopTrain(int train)
{
  switch (train)
  {
    case 1:
      train1Speed = 0;
      break;
    case 2:
      train2Speed = 0;
      break;
  }
}

void processDigitalLightSensor()
{
  int _light_state = digitalRead(_light_digital_pin);
  if (_light_state == HIGH)
  {
    if (train1auto)
    {
      train1Movement = stopping;
    }
  }
}

long analogLightSensorInterval = 500;
unsigned long analogLightSensorLastRead = 0;
void processAnalogLightSensor()
{
  unsigned long currentMillis = millis();
  if (currentMillis - analogLightSensorLastRead > analogLightSensorInterval)
  {
    analogLightSensorLastRead = currentMillis;
    int _light_value = analogRead(_light_analog_pin);
  }
}

int IRLow = 6;
int IRHigh = 10;
int IRStartStop = 19;
int IRMax = 32;
int IRPulse = 50;
int IRLength = 35;

word getLegoIRWord(decode_results results)
{
  word code = 0;
  //pause+start+16bits ignore stop
  if (results.rawlen >= IRLength)
  {
    //startbit
    int data = results.rawbuf[1] + results.rawbuf[2];
    if (data > IRStartStop && data < IRMax)
    {
      for (int b = 3; b < IRLength; b += 2)
      {
        data = results.rawbuf[b] + results.rawbuf[b + 1];
        if (data < IRLow)
        {
          return 0;
        }
        else if (data >= IRLow && data < IRStartStop)
        {
          code = code << 1;
          if (data >= IRHigh)
          {
            code = code | 1;
          }
        }
        else
        {
          return 0;
        }
      }
      //do checks
      int check = lowByte(code)^highByte(code);
      if (((check / 16 ^ check)&B00001111) == 15)
      {
        return code;
      }
    }
  }
  return 0;
}
int lasttoggle = 0;
void decodeLegoIR(word code)
{
  int toggle = (highByte(code) & B10000000) != 0;
  if (toggle != lasttoggle)
  {
    lasttoggle = toggle;
    if ((highByte(code) & B01000000) == 0)
    {
      //1xx
      if ((highByte(code) & B00000100) != 0)
      {
        legoSingleOutput(code);
      }
      //01x
      else if ((highByte(code) & B00000010) != 0)
      {
        //reserved
      }
      //001
      else if ((highByte(code) & B00000001) != 0)
      {
        //Combo Direct
      }
      //000
      else
      {
        //Extended
      }
    }
    else
    {
      //legoComboPWM
    }
  }
}

void legoSingleOutput(word code)
{
  unsigned int channel = ((highByte(code) >> 4 )& B00000011);
  unsigned int data = lowByte(code) >> 4;
  unsigned int mode = (highByte(code) & B00000010) != 0;
  unsigned int targetTrain = (highByte(code) & B00000001) != 0;
  /*Serial.print("[LEGO] >");
  Serial.print(" Word:");
  for (unsigned int mask = 0x8000; mask; mask >>= 1) {
    Serial.print(mask&code?'1':'0');
  }
  Serial.print(" Channel:");
  Serial.print(channel);
  Serial.print(" Data: ");
  Serial.print(data);
  Serial.print(" Mode: ");
  Serial.print(mode);
  Serial.print(" Target: ");
  Serial.print(targetTrain);
  Serial.println("");*/
  if(channel==0)
  {
    if (mode)
    {
      switch (data)
      {
        case 4:
          speedUpTrain(targetTrain + 1);
          break;
        case 5:
          slowDownTrain(targetTrain + 1);
          break;
      }
    }
    else
    {
      switch (data)
      {
        case 8:
          stopTrain(targetTrain + 1);
          break;
        default:
          break;
      }
    }
  }
}
